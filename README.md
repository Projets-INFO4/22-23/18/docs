# Portail pour gestionnaire des tâches sous Streamlit

INFO4 2022-2023 project.

- Ahmed Aziz KHELIFI
- Léa MAIDA
- Dorian MOUNIER
- Eloi CHARRA

## Project monitoring

### Week #1 

- discovery of streamlit
	- we started to create mini projects unrelated to [this project](https://gricad-gitlab.univ-grenoble-alpes.fr/Projets-INFO4/22-23/18) 
- look up for information about OAR
	- we searched information about OAR and more generally, HPC clusters in order to know what we were working on
- trying to start last year's project (react version)

### Week #2

- still discoving streamlit, building a simple interface
	- we created blank pages for the future interface according to the react version ones.
- fixing `oar-docker-compose` repo issues
	- we encountered an error in the `oar-docker-compose` github repository which prevented us to setup the environment

### Week #3

- starting to create the "Job" page
	- the `job` page is the most important one, we started to imitate the react version using streamlit
- trying to fix containers (backend) issues
	- we came across a new problem regarding the oar environment where docker containers could not start properly

### Week #4

- continuing "Job" page
- creating "Resources" page
- still trying to fix backend issues (github issue created)

### Week #5

- continuing pages with Streamlit
	- after `job` page, we started to create and build the other pages based on the existing project under react
- as we still have not access to the api, we are building pages with fake data raw coded
	- regarding the docker containers issue, we worked around the problem using real data we collected from the react interface. While the issue was being resolved, we were still able to continue the project
- preparing mid-term orals

### Week #6

- implementing api access
	- creating a global endpoint for all API calls of our application
- linking pages to the API
	- connecting previously done pages to the API (now that we have access to it)
- started working on Monikas

### Week #7

- still impleting pages linked to the API
- troubleshooting api issues (forbidden access to POST requests)
	- we came across a new issue where we could not send POST requests to the API (fixed)

### Week #8

- after having POST requests working, job submitting works
- working on resources as well
	- trying to figure out how to use Streamlit in an interactive way (dynamic tables)
- Monikas implementation done
	- as we get an HTML response from the API, we are thinking about displaying it differently by scraping the HTML response

### Week #9

- resources page done
  - resources are editable (modify the state, delete...)
  - issues with data display (where to show information)
- project cleanup
  - renaming files and folder for better readability
- fix drawgantt and monika display
  - change the display to be an HTML `iframe` like the react version (mandatory to have actions working)
- authenication linked to job and resources page
